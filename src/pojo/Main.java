package pojo;

public class Main {

	public static void main(String[] args) {
		// Using thread
		try {
			Initialization object = new Initialization();
			Thread thread=new Thread(object);
			thread.start();

		    }
		catch (Exception e)
		{
			System.out.println(e);
		}

	}
}
