package pojo;


	public class MagicOfBooks {
		User obju = new User();
		Books objb= new Books();

		// will check for user name is it allowed or not
		public boolean checkUser() {
			int count = 0;

			for (String s : obju.userNames) {

				if (s.toLowerCase().equals(Inputs.input_users.toLowerCase())) 
				{
					Inputs.output_users = s;
					count++;
				}

			}
			if (count == 0) {
				return false;
			} else {
				return true;
			}

		}

		// to select option
		public void opt() {
			try {
				System.out.println("**************MENU**************");
				System.out.println("1.Print your books(new,favourite,completed)");
				System.out.println("2.Find Book by bookid");
				System.out.println("3.Print the details of a Book");
				System.out.println("Please enter your choice");

				int opt = Integer.parseInt(Inputs.in.nextLine());
				// if option is not from 1-3
				while (opt > 3 | opt < 0) {
					System.out.println("Please enter your options between 1-3");
					System.out.println("Please enter your choice");
					opt = Integer.parseInt(Inputs.in.nextLine());
				}
				switch (opt) {

				case 1:
					info();
					break;
				case 2:
					findbookid();
					break;
				case 3:
					findBook();
					break;

				}
			} 
			catch (Exception e) {
				System.out.println("You didn't entered or entered wrong syntax of option");
				System.exit(1);

			}
		}

		// will find the Book id present or not in our collection
		public void findbookid() {
			try {
				int count = 0;
				System.out.println("Please enter your Book id:");
				int bookid = Integer.parseInt(Inputs.in.nextLine());
				// searching for Book ids in Book id array
				for (int id : objb.bookId) {
					if (bookid == id) {
						count++;
					}
				}
				if (count == 0) {
					System.out.println("Unavailaible");
				} else {
					System.out.println("Available");
				}
			} 
			catch (Exception e) {
				System.out.println("you didn't entered or entered wrong syntax of the Book id");
				System.exit(1);
			}

		}

		// print about users books
		public void info() {
			obju.setdetails(Inputs.input_users);
			// splitting books name by ,
			String[] newbook = obju.user_details.get(Inputs.input_users).get("New Books:").split(",");
			String[] compbook = obju.user_details.get(Inputs.input_users).get("Completed Books:").split(",");
			String[] favbook = obju.user_details.get(Inputs.input_users).get("Favourite Books:").split(",");
			System.out.println("New Books:");
			for (String book : newbook)

			{
				System.out.println(book);
			}
			System.out.println("Favourite Books:");
			for (String book : favbook)

			{
				System.out.println(book);
			}
			System.out.println("Completed Books:");
			for (String book : compbook)

			{
				System.out.println(book);
			}

		}

		// will search for Book name in our collection
		public void findBook() {
			try {
				int i = 0;
				int count = 0;
				System.out.println("Enter Book name: ");
				Inputs.input_books = Inputs.in.nextLine();
				// will search for book_name in books name array
				for (String b : objb.books_Name) {
					if (b.toLowerCase().equals(Inputs.input_books.toLowerCase())) {
						System.out.println("Author name: " + objb.authors_Name[i]);
						System.out.println("Description: " + objb.description[i]);
						count++;
					}
					i++;
				}
				if (count == 0) {
					System.out.println("Book not found!");
				}
			}
			// if Book name space is empty
			catch (Exception e) {
				System.out.println("You didn't entered the Book name");
				System.exit(1);
			}

		}

		public void torun() {
			try {

			System.out.println("Do you want to continue?");
			Inputs.contin = Inputs.in.nextLine();

			while (!Inputs.contin.toLowerCase().equals("yes") && !Inputs.contin.toLowerCase().equals("no")) {
				System.out.println("Please enter your answer with these 2 values : yes OR no");
				System.out.println("Do you want to continue?");
				Inputs.contin = Inputs.in.nextLine();
			}
			if (Inputs.contin.toLowerCase().equals("yes")) {
				Thread.currentThread().run();
			}
			}
			catch(Exception e)
			{
				System.out.println("You didnt answered the question: Do you want to continue? ");
			}

		}

	}

