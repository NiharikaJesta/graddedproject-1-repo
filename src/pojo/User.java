package pojo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

	public class User {
		String[] userNames = { "Neeha", "Sunil", "Hari", "Sujatha", "Munna" };
		private String user_Id;
		private String email_Id;
		private String password;
		//Mapping String and ArrayList for books using HashMap
		Map<String, ArrayList<Books>> Book = new HashMap<String, ArrayList<Books>>();
		Map<String, HashMap<String, String>> user_details = new HashMap<String, HashMap<String, String>>();

		public void setdetails(String users) {
			HashMap<String, String> details = new HashMap<String, String>();
			//Using switch case to select particular user
			switch (users.toLowerCase()) {
			case "Neeha":
				details.put("New Books:", "Ulysses");
				details.put("Favourite Books:", "Don Quixote,One Hundred Years of Solitude");
				details.put("Completed Books:", "Hamlet");
				break;
			case "Sunil":
				details.put("New Books:", "One Hundred Years of Solitude");
				details.put("Favourite Books:", "Ulysses,Hamlet");
				details.put("Completed Books:", "In Search of Lost Time");
				break;
			case "Hari":
				details.put("New Books:", "Don Quixote");
				details.put("Favourite Books:", "One Hundred Years of Solitude");
				details.put("Completed Books:", "Hamlet,In Search of Lost Time");
				break;
			case "Sujatha":
				details.put("New Books:", "Ulysses,In Search of Lost Time");
				details.put("Favourite Books:", "Don Quixote");
				details.put("Completed Books:", "Don Quixote,Hamlet");
				break;
			case "Munna":
				details.put("New Books:", "In Search of Lost Time");
				details.put("Favourite Books:", "Ulysses");
				details.put("Completed Books:", "Hamlet");
				break;
			}
	        //Mapping user and details into user_details
			user_details.put(users, details);


	}

	}



